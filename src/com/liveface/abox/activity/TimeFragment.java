package com.liveface.abox.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liveface.abox.R;
import com.liveface.abox.view.DigitalClock;
import com.liveface.abox.view.MyDialog;
import com.liveface.abox.view.SJDigitalClock;

public class TimeFragment extends Fragment implements OnClickListener {
	private TextView tv_t1, tv_t2, tv_t3;
	private LinearLayout lin_sj, ll_t1, ll_t2, ll_t3;
	private SJDigitalClock dc_t1, dc_t2, dc_t3;
	private String mt1, mt2, mt3, mn1, mn2, mn3;
	private ImageView iv_add;
	private int num,WIDTH,HIGHT;
	private Typeface typeFace;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_time, null, false);
		typeFace=Typeface.createFromAsset(getActivity().getAssets(),"fonts/AvantGardeTwoBQ-Book.ttf");
		tv_t1 = (TextView) view.findViewById(R.id.tv_t1);
		tv_t2 = (TextView) view.findViewById(R.id.tv_t2);
		tv_t3 = (TextView) view.findViewById(R.id.tv_t3);
		ll_t1 = (LinearLayout) view.findViewById(R.id.ll_t1);
		ll_t1.setOnClickListener(this);
		ll_t2 = (LinearLayout) view.findViewById(R.id.ll_t2);
		ll_t2.setOnClickListener(this);
		ll_t3 = (LinearLayout) view.findViewById(R.id.ll_t3);
		ll_t3.setOnClickListener(this);
		dc_t1 = (SJDigitalClock) view.findViewById(R.id.dc_t1);
		dc_t2 = (SJDigitalClock) view.findViewById(R.id.dc_t2);
		dc_t3 = (SJDigitalClock) view.findViewById(R.id.dc_t3);
		iv_add = (ImageView) view.findViewById(R.id.iv_add);
		lin_sj = (LinearLayout) view.findViewById(R.id.lin_sj);
		lin_sj.setOnClickListener(this);
		DigitalClock tv_time=(DigitalClock) view.findViewById(R.id.tv_time);
		tv_time.setTypeface(typeFace);
		dc_t1.setTypeface(typeFace);
		dc_t2.setTypeface(typeFace);
		dc_t3.setTypeface(typeFace);
		tv_t1.setTypeface(typeFace);
		tv_t2.setTypeface(typeFace);
		tv_t3.setTypeface(typeFace);
		panduan();
		DisplayMetrics metric = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metric);
		WIDTH = metric.widthPixels;
		HIGHT=metric.heightPixels;
		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ll_t1:
			tishi(1);
			break;
		case R.id.ll_t2:
			tishi(2);
			break;
		case R.id.ll_t3:
			tishi(3);
			break;
		case R.id.lin_sj:
			if (num == 3) {
				Toast.makeText(getActivity(), "已添加满，暂无法添加",
						Toast.LENGTH_SHORT).show();
			} else {
				Intent intent = new Intent(getActivity(),
						XuanZeActivity.class);
				intent.putExtra("num", num);
				startActivityForResult(intent, 11);
			}
			break;

		default:
			break;
		}
	}

	private void panduan() {
		mt1 = getActivity()
				.getSharedPreferences("mytime1", Context.MODE_PRIVATE)
				.getString("mt1", " ");
		mt2 = getActivity()
				.getSharedPreferences("mytime2", Context.MODE_PRIVATE)
				.getString("mt2", " ");
		mt3 = getActivity()
				.getSharedPreferences("mytime3", Context.MODE_PRIVATE)
				.getString("mt3", " ");
		mn1 = getActivity()
				.getSharedPreferences("mytime1", Context.MODE_PRIVATE)
				.getString("mn1", " ");
		mn2 = getActivity()
				.getSharedPreferences("mytime2", Context.MODE_PRIVATE)
				.getString("mn2", " ");
		mn3 = getActivity()
				.getSharedPreferences("mytime3", Context.MODE_PRIVATE)
				.getString("mn3", " ");
		if (mt1.equals(" ")) {
			ll_t1.setVisibility(View.GONE);
			ll_t2.setVisibility(View.GONE);
			ll_t3.setVisibility(View.GONE);
			iv_add.setVisibility(View.VISIBLE);
			num = 0;
		} else if (mt2.equals(" ")) {
			ll_t1.setVisibility(View.VISIBLE);
			ll_t2.setVisibility(View.GONE);
			ll_t3.setVisibility(View.GONE);
			iv_add.setVisibility(View.VISIBLE);
			dc_t1.setShiqu(mt1);
			tv_t1.setText(mn1);
			num = 1;
		} else if (mt3.equals(" ")) {
			ll_t1.setVisibility(View.VISIBLE);
			ll_t2.setVisibility(View.VISIBLE);
			ll_t3.setVisibility(View.GONE);
			iv_add.setVisibility(View.VISIBLE);
			dc_t1.setShiqu(mt1);
			tv_t1.setText(mn1);
			dc_t2.setShiqu(mt2);
			tv_t2.setText(mn2);
			num = 2;
		} else {
			ll_t1.setVisibility(View.VISIBLE);
			ll_t2.setVisibility(View.VISIBLE);
			ll_t3.setVisibility(View.VISIBLE);
			iv_add.setVisibility(View.GONE);
			dc_t1.setShiqu(mt1);
			tv_t1.setText(mn1);
			dc_t2.setShiqu(mt2);
			tv_t2.setText(mn2);
			dc_t3.setShiqu(mt3);
			tv_t3.setText(mn3);
			num = 3;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 11) {
			if (resultCode == -1) {
				num = data.getExtras().getInt("num");
				panduan();
			}
		}
	}
	
	private void tishi(final int sj){
		View view1=getActivity().getLayoutInflater().inflate(R.layout.items_dialog_simap, null);
		TextView tv_msg=(TextView) view1.findViewById(R.id.tv_msg);
		TextView tv_ok=(TextView) view1.findViewById(R.id.tv_ok);
		TextView tv_no=(TextView) view1.findViewById(R.id.tv_no);
		final MyDialog  dialog1 = new MyDialog(getActivity(), R.style.CustomDialog);
		LinearLayout.LayoutParams lp1=new LinearLayout.LayoutParams(WIDTH*4/5,LinearLayout.LayoutParams.WRAP_CONTENT);
		dialog1.addContentView(view1, lp1);
		dialog1.setCancelable(true);
		dialog1.show();
		tv_msg.setText("是否删除该世界时间？");
		tv_no.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dialog1.dismiss();
			}
		});
		tv_ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (sj==1) {
					getActivity().getSharedPreferences("mytime1",Context.MODE_PRIVATE).edit().clear().commit();
					getActivity().getSharedPreferences("mytime1", Context.MODE_PRIVATE).edit().putString("mt1", mt2).putString("mn1", mn2).commit();
					getActivity().getSharedPreferences("mytime2", Context.MODE_PRIVATE).edit().putString("mt2", mt3).putString("mn2", mn3).commit();
					getActivity().getSharedPreferences("mytime3",Context.MODE_PRIVATE).edit().clear().commit();
				}else if (sj==2) {
					getActivity().getSharedPreferences("mytime2",Context.MODE_PRIVATE).edit().clear().commit();
					getActivity().getSharedPreferences("mytime2", Context.MODE_PRIVATE).edit().putString("mt2", mt3).putString("mn2", mn3).commit();
					getActivity().getSharedPreferences("mytime3",Context.MODE_PRIVATE).edit().clear().commit();
				}else if (sj==3) {
					getActivity().getSharedPreferences("mytime3",Context.MODE_PRIVATE).edit().clear().commit();
				}
				dialog1.dismiss();
				panduan();
			}
		});
	}
	
}
