package com.liveface.abox.activity;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.liveface.abox.R;

public class MainActivity extends FragmentActivity implements OnClickListener {
	private ViewPager viewPager;
	private TextView tv_toptitle;
	private ImageView iv_timez, iv_calculator, iv_compass;
	private Fragment[] fragments = { new TimeFragment(),
			new CalculatorFragment(), new CompassFragment() };
	private List<Fragment> listFragment = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		viewPager = (ViewPager) findViewById(R.id.view_containt);
		iv_timez = (ImageView) findViewById(R.id.iv_timez);
		iv_timez.setOnClickListener(this);
		tv_toptitle = (TextView) findViewById(R.id.tv_toptitle);
		iv_calculator = (ImageView) findViewById(R.id.iv_calculator);
		iv_calculator.setOnClickListener(this);
		iv_compass = (ImageView) findViewById(R.id.iv_compass);
		iv_compass.setOnClickListener(this);
		listFragment = new ArrayList<Fragment>();
		tv_toptitle.setText(getResources().getString(R.string.timezone));
		iv_timez.setSelected(true);
		;
		iv_calculator.setSelected(false);
		iv_compass.setSelected(false);
		for (int i = 0; i < fragments.length; i++) {
			Fragment fg = fragments[i];
			listFragment.add(fg);
		}
		viewPager.setAdapter(new MyAdapterFragment(getSupportFragmentManager()));
		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				if (arg0 == 0) {
					tv_toptitle.setText(getResources().getString(
							R.string.timezone));
					iv_timez.setSelected(true);
					;
					iv_calculator.setSelected(false);
					iv_compass.setSelected(false);
				} else if (arg0 == 1) {
					tv_toptitle.setText(getResources().getString(
							R.string.Calculator));
					iv_timez.setSelected(false);
					;
					iv_calculator.setSelected(true);
					iv_compass.setSelected(false);
				} else if (arg0 == 2) {
					tv_toptitle.setText(getResources().getString(
							R.string.Compass));
					iv_timez.setSelected(false);
					;
					iv_calculator.setSelected(false);
					iv_compass.setSelected(true);
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		;

	}

	class MyAdapterFragment extends FragmentPagerAdapter {

		public MyAdapterFragment(FragmentManager fm) {

			super(fm);
		}

		@Override
		public Fragment getItem(int arg0) {
			return listFragment.get(arg0);
		}

		@Override
		public int getCount() {
			return listFragment.size();
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_timez:
			viewPager.setCurrentItem(0);
			break;
		case R.id.iv_calculator:
			viewPager.setCurrentItem(1);
			break;
		case R.id.iv_compass:
			viewPager.setCurrentItem(2);
			break;

		default:
			break;
		}

	}
}
