package com.liveface.abox.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class XuanZeActivity extends Activity {
	private String[] items={"��;��","����ɽ","��������","��ɼ��","�Ừ��","��˳�","������","����","��˹�����","֥�Ӹ�","ī����","�����","�����",
			"ŦԼ","������˹","�ͰͶ�˹","���˹","ʥԼ��","ʥ���Ǹ�","����������","����ŵ˹����˹","���ػ���","������","��������","������Ⱥ��","��ý�","����������",
			"�׶�","��Ī˹�ص�","����������","��³����","��������","������ά","�µúͿ�","����","�ŵ�","��³��","����","�ն�����","Ү·����","������",
			"��˹��","�͸��","������","���ޱ�","�º���","Ī˹��","�Ϳ�","�ڱ���˹","������","�ϰ�","������","������","������","�Ӷ�����",
			"������","�Ჴ��ʱ��","Ҷ�����ձ�","����ľͼ","���ʱ��","����","����˹ŵ�Ƕ�˹��","����","���","��¡��","��˹","̨��","������Ŀ�","�׶�",
			"����","�����","�ſ�Ŀ�","����˹��","�ص�","��������","������","Ϥ��","������","������","���ӵ�","쳼�","�¿���","��������"};
	private String[] items2={"GMT-11:00","GMT-10:00","GMT-9:00","GMT-8:00","GMT-8:00","GMT-7:00","GMT-7:00","GMT-7:00","GMT-6:00","GMT-6:00","GMT-6:00","GMT-6:00","GMT-5:00",
			"GMT-5:00","GMT-4:30","GMT-4:00","GMT-4:00","GMT-3:30","GMT-3:00","GMT-3:00","GMT-3:00","GMT-3:00","GMT-2:00","GMT-2:00","GMT-1:00","GMT-1:00","GMT+0:00",
			"GMT+0:00","GMT+1:00","GMT+1:00","GMT+1:00","GMT+1:00","GMT+1:00","GMT+2:00","GMT+2:00","GMT+2:00","GMT+2:00","GMT+2:00","GMT+2:00","GMT+2:00","GMT+2:00",
			"GMT+3:00","GMT+3:00","GMT+3:00","GMT+3:00","GMT+3:30","GMT+4:00","GMT+4:00","GMT+4:00","GMT+4:00","GMT+4:00","GMT+4:30","GMT+5:00","GMT+5:00","GMT+5:30",
			"GMT+5:30","GMT+5:45","GMT+6:00","GMT+6:00","GMT+6:30","GMT+7:00","GMT+8:00","GMT+8:00","GMT+8:00","GMT+8:00","GMT+8:00","GMT+8:00","GMT+9:00","GMT+9:00",
			"GMT+9:00","GMT+9:30","GMT+10:00","GMT+10:00","GMT+10:00","GMT+10:30","GMT+11:00","GMT+11:00","GMT+11:00","GMT+12:00","GMT+12:00","GMT+12:00","GMT+13:00","GMT+13:00"};
	private int num;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ListView lv=new ListView(XuanZeActivity.this);
		setContentView(lv);
		num=getIntent().getExtras().getInt("num");
		lv.setAdapter(new MyAdapter2());
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (num==0) {
					getSharedPreferences("mytime1", Context.MODE_PRIVATE).edit().putString("mt1", items2[position]).putString("mn1", items[position]).commit();
					num++;
				}
				else if (num==1) {
					getSharedPreferences("mytime2", Context.MODE_PRIVATE).edit().putString("mt2", items2[position]).putString("mn2", items[position]).commit();
					num++;
				}
				else if (num==2) {
					getSharedPreferences("mytime3", Context.MODE_PRIVATE).edit().putString("mt3", items2[position]).putString("mn3", items[position]).commit();
					num++;
				}
				Intent data=new Intent();
				data.putExtra("num", num);
			    setResult(RESULT_OK,data);
				finish();
			}
		});
	
	}
	class MyAdapter2 extends BaseAdapter{

		@Override
		public int getCount() {
			return items.length;
		}

		@Override
		public Object getItem(int position) {
			return null;
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout lin=new LinearLayout(XuanZeActivity.this);
			lin.setOrientation(LinearLayout.VERTICAL);
			lin.setPadding(15, 3, 0, 3);
			TextView tv1=new TextView(XuanZeActivity.this); 
			TextView tv2=new TextView(XuanZeActivity.this);
			tv1.setTextSize(20);
			tv1.setTextColor(Color.WHITE);
			tv1.setText(items[position]);
			tv2.setTextSize(12);
			tv2.setTextColor(Color.LTGRAY);
			tv2.setText(items2[position]);
			lin.addView(tv1);
			lin.addView(tv2);
			return lin;
		}
		
	}	
}
