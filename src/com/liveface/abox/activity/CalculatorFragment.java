package com.liveface.abox.activity;

import com.liveface.abox.R;

import android.R.raw;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CalculatorFragment extends Fragment {
	private TextView led;
	private Button one, two, three, four, five, six, seven, eight, nine, zero,
			dot, add, sub, mult, division, amount, clear,pow,sqrt;
	private String num1 = "";
	private String num2 = "";
	private double num3 = 0;
	private String rezult = "";
	private String sign = null;
	private int mark = 0;
	private boolean flag = true;
	private boolean dotmark = true;
	private Typeface typeFace;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_calculator, null);
		typeFace=Typeface.createFromAsset(getActivity().getAssets(),"fonts/AvantGardeTwoBQ-Book.ttf");
		led = (TextView) view.findViewById(R.id.tv_reson);
		init(view);
		return view;
	}

	private void init(View view) {
		zero = (Button) view.findViewById(R.id.btn_0);
		one = (Button) view.findViewById(R.id.btn_1);
		two = (Button) view.findViewById(R.id.btn_2);
		three = (Button) view.findViewById(R.id.btn_3);
		four = (Button) view.findViewById(R.id.btn_4);
		five = (Button) view.findViewById(R.id.btn_5);
		six = (Button) view.findViewById(R.id.btn_6);
		seven = (Button) view.findViewById(R.id.btn_7);
		eight = (Button) view.findViewById(R.id.btn_8);
		nine = (Button) view.findViewById(R.id.btn_9);
		dot = (Button) view.findViewById(R.id.btn_di);
		amount = (Button) view.findViewById(R.id.btn_de);
		add = (Button) view.findViewById(R.id.btn_up);
		sub = (Button) view.findViewById(R.id.btn_down);
		mult = (Button) view.findViewById(R.id.btn_adva);
		division = (Button) view.findViewById(R.id.btn_divid);
		clear = (Button) view.findViewById(R.id.btn_ac);
		pow = (Button) view.findViewById(R.id.btn_pow);
		sqrt = (Button) view.findViewById(R.id.btn_sqrt);
//		led.setTypeface(typeFace);
		zero.setTypeface(typeFace);
		one.setTypeface(typeFace);
		two.setTypeface(typeFace);
		three.setTypeface(typeFace);
		four.setTypeface(typeFace);
		five.setTypeface(typeFace);
		six.setTypeface(typeFace);
		seven.setTypeface(typeFace);
		eight.setTypeface(typeFace);
		nine.setTypeface(typeFace);
		dot.setTypeface(typeFace);
		amount.setTypeface(typeFace);
		add.setTypeface(typeFace);
		sub.setTypeface(typeFace);
		mult.setTypeface(typeFace);
		division.setTypeface(typeFace);
		clear.setTypeface(typeFace);
		pow.setTypeface(typeFace);
		sqrt.setTypeface(typeFace);
		zero.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "0";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "0";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "0";
					led.setText(num1 + sign + num2);
				}
			}
		});
		one.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "1";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "1";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "1";
					led.setText(num1 + sign + num2);
				}
			}
		});
		two.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "2";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "2";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "2";
					led.setText(num1 + sign + num2);
				}
			}
		});
		three.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "3";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "3";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "3";
					led.setText(num1 + sign + num2);
				}
			}
		});
		four.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "4";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "4";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "4";
					led.setText(num1 + sign + num2);
				}
			}
		});
		five.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "5";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "5";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "5";
					led.setText(num1 + sign + num2);
				}
			}
		});
		six.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "6";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "6";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "6";
					led.setText(num1 + sign + num2);
				}
			}
		});
		seven.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "7";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "7";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "7";
					led.setText(num1 + sign + num2);
				}
			}
		});
		eight.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "8";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "8";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "8";
					led.setText(num1 + sign + num2);
				}
			}
		});
		nine.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0) {
					num1 = num1 + "9";
					led.setText(num1);
				} else if (mark == 1) {
					num2 = num2 + "9";
					led.setText(num1 + sign + num2);
				} else {
					num1 = String.valueOf(num3);
					num2 = num2 + "9";
					led.setText(num1 + sign + num2);
				}
			}
		});
		add.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (flag) {
					sign = "+";
					mark++;
					flag = false;
					dotmark = true;
					led.setText(led.getText()+sign);
				}
			}
		});
		sub.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (flag) {
					sign = "-";
					mark++;
					flag = false;
					dotmark = true;
					led.setText(led.getText()+sign);
				}
			}
		});
		mult.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (flag) {
					sign = "*";
					mark++;
					flag = false;
					dotmark = true;
					led.setText(led.getText()+sign);
				}
			}
		});
		division.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (flag) {
					sign = "÷";
					mark++;
					flag = false;
					dotmark = true;
					led.setText(led.getText()+sign);
				}
			}
		});
		dot.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (dotmark) {
					if (mark == 0) {
						num1 = num1 + ".";
						led.setText(num1);
					} else if (mark == 1) {
						num2 = num2 + ".";
						led.setText(num1 + sign + num2);
					} else {
						num1 = String.valueOf(num3);
						num2 = num2 + ".";
						led.setText(num1 + sign + num2);
					}
					dotmark = false;
				}
			}
		});
		amount.setOnClickListener(new OnClickListener() { // 等于时发生的运算
			public void onClick(View v) {
				flag = true;
				dotmark = true;
				if (sign.equals("+")) {
					try {
						double x = Double.parseDouble(num1);
						double y = Double.parseDouble(num2);
						num3 = x + y;
						rezult = String.valueOf(num3);
						led.setText(num1 + sign + num2 + "\n" + rezult);
						num2 = "";
					} catch (Exception e) {
						Toast.makeText(getActivity(), "请确定您计算参数的正确性", Toast.LENGTH_SHORT).show();
					}
				
				} else if (sign.equals("-")) {
					try {
						double x = Double.parseDouble(num1);
						double y = Double.parseDouble(num2);
						num3 = x - y;
						rezult = String.valueOf(num3);
						led.setText(num1 + sign + num2 + "\n" + rezult);
						num2 = "";
					} catch (Exception e) {
						Toast.makeText(getActivity(), "请确定您计算参数的正确性", Toast.LENGTH_SHORT).show();
					}
				
				} else if (sign.equals("*")) {
					try {
						double x = Double.parseDouble(num1);
						double y = Double.parseDouble(num2);
						num3 = x * y;
						rezult = String.valueOf(num3);
						led.setText(num1 + sign + num2 + "\n" + rezult);
						num2 = "";
					} catch (Exception e) {
						Toast.makeText(getActivity(), "请确定您计算参数的正确性", Toast.LENGTH_SHORT).show();
					}
				
				} else if (sign.equals("÷")) {
					try {
						double x = Double.parseDouble(num1);
						double y = Double.parseDouble(num2);
						num3 = x / y;
						rezult = String.valueOf(num3);
						led.setText(num1 + sign + num2 + "\n" + rezult);
						num2 = "";
					} catch (Exception e) {
						Toast.makeText(getActivity(), "请确定您计算参数的正确性", Toast.LENGTH_SHORT).show();
					}
					
				} else if (sign.equals("^")) {
					try {
						double x = Double.parseDouble(num1);
						double y = Double.parseDouble(num2);
						num3 = Math.pow(x, y);
						rezult = String.valueOf(num3);
						led.setText(num1 + sign + num2 + "\n" + rezult);
						num2 = "";
					} catch (Exception e) {
						Toast.makeText(getActivity(), "请确定您计算参数的正确性", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
		clear.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				num1 = "";
				num2 = "";
				num3 = 0;
				mark = 0;
				sign = "";
				led.setText("0");
				flag = true;
				dotmark = true;
			}
		});
		pow.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (flag) {
					sign = "^";
					mark++;
					flag = false;
					dotmark = true;
					led.setText(led.getText()+sign);
				}
			}
		});
		sqrt.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (mark == 0 && !num1.equals("") && num2.equals("")) {
					double parent = Double.parseDouble(num1);
					double root = Math.sqrt(parent);
					rezult = String.valueOf(root);
					led.setText("√"+parent+"\n"+rezult);
					num1 = "";
				}
			}
		});

	}

}
